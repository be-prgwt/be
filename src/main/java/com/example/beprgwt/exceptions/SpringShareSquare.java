package com.example.beprgwt.exceptions;

public class SpringShareSquare extends RuntimeException{
    public SpringShareSquare(String exMessage, Exception exception) {
        super(exMessage, exception);
    }

    public SpringShareSquare(String exMessage) {
        super(exMessage);
    }
}
