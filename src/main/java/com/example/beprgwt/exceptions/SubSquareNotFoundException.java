package com.example.beprgwt.exceptions;

public class SubSquareNotFoundException extends RuntimeException {
    public SubSquareNotFoundException(String message) {
        super(message);
    }
}
