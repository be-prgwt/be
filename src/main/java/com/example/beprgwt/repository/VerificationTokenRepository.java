package com.example.beprgwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VerificationTokenRepository<VerificationToken> extends JpaRepository<VerificationToken, Long> {
    <VerificationToken> Optional<VerificationToken> findByToken(String token);
}
