package com.example.beprgwt.repository;

import com.example.beprgwt.model.Comment;
import com.example.beprgwt.model.Post;
import com.example.beprgwt.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findByPost(Post post);

    List<Comment> findAllByUser(User user);
}