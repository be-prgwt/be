package com.example.beprgwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BePrgwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(BePrgwtApplication.class, args);
    }

}
